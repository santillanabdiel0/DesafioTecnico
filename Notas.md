# Ejecutar la Parte Visual del Proyecto
## Contenido
- [Instalación y Ejecución](#instalación-y-ejecución)
- [Descripción](#descripción)

# Instalación y Ejecución 
Antes de empezar, asegúrate de tener Node >= 14 y NPM >= 6.14.12 o Yarn >= 1.21.1 instalados en tu máquina. 
1. Ingresa a la carpeta "notesAppFrontEnd" y abre el símbolo del sistema para ejecutar los siguientes comandos:
```
 # Instalar Dependencias con Yarn:
  $ yarn install
```
2.Una vez instalado, ejecuta el servidor de desarrollo de Quasar:
````
  # Iniciar el servidor de Desarrollo
  $ yarn dev
````
En la ventana del símbolo del sistema aparecerá información y los enlaces donde se encuentra el proyecto.

# Descripción
## Explicación del Funcionamiento de la Aplicación

### Página Principal - Diseño Inspirado en "Keep"

![Pagina principal](Images/Pantallaprincipal.png)

### Agregar una Nota Nueva
Al presionar el input que dice "Crear una nota...", aparecerá el siguiente input.
El botón en la esquina superior derecha permite agregar la nota, y al guardarla se creará la nota.
El tercer botón de izquierda a derecha archiva las notas, lo cual las enviará directamente a la sección de archivado.

![Inputa para agregar notas](Images/Inputagregarnota.png)

### Notas Archivadas
Al navegar por el panel izquierdo y seleccionar la funcionalidad de archivar, se mostrarán todas las notas archivadas.

![Archivar una nota](Images/Paginadearchivado.png)

### Editar una Nota
Es posible editar una nota sin importar si está fijada o archivada.
Al hacer clic en la nota, se desplegará una ventana emergente con la información de la nota que se desea editar.
También se puede fijar o archivar la nota desde esta ventana emergente.

![Editar una nota](Images/Inputemergente.png)

### Eliminar una Nota

Para eliminar una nota, coloca el ratón sobre la nota y aparecerán los botones de la nota a la que estás apuntando con el cursor.
Presiona el primer botón de derecha a izquierda, el cual despliega un menú, y luego selecciona "Borrar la nota".
Así se eliminará la nota de la base de datos.

![Borrar una nota](Images/Menuemergente.png)


