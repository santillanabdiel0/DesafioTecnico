# Cómo Correr la API
## Contenido
- [Descripción](#descripción)
- [Instalación y Ejecución](#instalación-y-ejecución)

# Descripción
Esta es una API REST creada en Laravel 9 utilizando el patrón de diseño MVC (Modelo Vista Controlador) para mantener un orden en el proyecto.

## Endpoints de la api
| Metodo    | EndPoint                         | Descripcion |
|-----------|----------------------------------|-----------|
| GET       | /api/v1/notes                    |Paginar notas, ordenadas por fecha de creación y primero las fijadas   |
| POST      | /api/v1/notes                    | Crear notas |
| DELETE    | /api/v1/notes/{{id}}             | Eliminar nota enviando por url el ID de la nota    |
| PUT       | /api/v1/notes/{{id}}             | Modificar nota    |
| POST      | /api/v1/notes/{{id}}/archived    | Archivar nota    |
| GET       | /api/v1/notes/archived           | Paginar notas que han sido archivadas  |

# Instalación y Ejecución
Antes de empezar, asegúrate de que tu máquina local tenga PHP y  [Composer](https://getcomposer.org/) instalados.
Una vez que hayas descargado el proyecto, sigue estos pasos para la instalación y ejecución:

1. Abre una terminal o símbolo del sistema y navega hasta la carpeta del proyecto ./notes-app.
2. Ejecuta el siguiente comando para instalar todas las dependencias del proyecto:
```
# Instalar Dependencias:
$ composter install

```
3. Antes de correr el servidor, se tiene que migrar la base de datos. Para ello, debes configurar unos archivos para que funcione en tu máquina. Abre el archivo .env que se encuentra en la ruta /notas-app/.env y busca las siguientes líneas de código:
```
  DB_CONNECTION=mysql   // Aquí se especifica el tipo de conexión; como estamos usando MySQL, lo dejamos así
  DB_HOST=127.0.0.1     // Aquí va el host; como se corre el proyecto de manera local, así se queda
  DB_PORT=3306          // Aquí va el puerto de la base de datos; como usamos XAMPP, viene por defecto en este puerto
  DB_DATABASE=bdnotas   // Aquí va el nombre de la base de datos que vamos a utilizar
  DB_USERNAME=root      // El usuario con el que vamos a crear la conexión
  DB_PASSWORD=          // Aquí va tu contraseña; si no tienes una, puedes dejarla en blanco

```
4. Guarda los cambios en el archivo .env y luego ejecuta el siguiente comando en el símbolo del sistema:
```
  #Migracoin para crear las tablas necesarias en la base de datos
  $ php artisan migrate
```
5. Finalmente, inicia el servidor de desarrollo de Laravel ejecutando el siguiente comando:
```
  #Iniciar el servidaor
  $ php artisan serve
```
Esto iniciará el servidor en la dirección http://127.0.0.1:8000. Ahora podrás acceder y probar la API.
