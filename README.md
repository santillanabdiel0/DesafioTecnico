# Mi Propia Versión de Keep: Organizador de Notas
Prueba técnica de conocimientos
## Tabla de contenidos
- [Descripción](#descripcion)
- [Instalación y Ejecución](#intallation-and-run)
- [Créditos](#creditos)


## Descripción
Esta aplicación ha sido visualmente recreada inspirándose en "Keep". Su funcionalidad principal radica en la capacidad de crear, archivar, editar, fijar y borrar notas. Además, estas notas se presentan en forma de tarjetas, y se les brinda la opción de acceso para realizar ediciones en su contenido.

La información se almacena en una base de datos MySQL y se accede a través de una API Restful, lo que permite una interacción eficiente y segura con los datos.

## Instalación y Ejecución
### Configuración de desarrollo
Para el funcionamiento completo de este proyecto, es necesario ejecutar la API creada en Laravel y también se debe ejecutar Quasar para poder consultar la API.
- [Configuración del backend](api.md)
- [Configuración del Frontend](Notas.md)

## Creditos
- [J3fte](santillanabdiel0@gmail.com)
